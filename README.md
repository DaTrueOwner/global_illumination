
Dependencies: 
    - Premake5 (Lua Build System)
    - Assimp (Asset loading library)
    - OpenGL 
    - GLFW (Window management)
    - GLM (Vector Math)
    -> Gui library is embedded (ImGui)
    -> OpenGL function loader library is embedded (glad)

My Premake5 script requires you to provide information where to find the libraries. Therefore you have to create a local file called "library_paths.txt" that is placed inside the project root with the following information and format:
    
    
    bin-/path/to/your/bin/directory
    lib-/path/to/your/lib/directory
    include-/path/to/your/include/directory
    

then just cd into the root directory and type "premake5 {your preferred backend}". (e.g. premake5 vs2017 or premake5 codelite)

---------------------------------
SCREENSHOTS:
---------------------------------
![Alt text](screenshots/svo.jpg "Sparse Voxel Octree of a mesh")

 
