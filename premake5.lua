includedir = nil
bindir = nil
libdir = nil

function load_library_paths()
    local f = os.locate("library_paths.txt")
    if f == nil then
        premake.error("Could not locate library_paths.txt. This file is needed to search for libraries. See README.md") 
        os.exit()
    end
    local content = io.readfile(f)
    bindir = nil
    libdir = nil
    includedir = nil
    for key,item in next,content:explode("\n") do
        item = item:match( "^%s*(.+)" ) -- remove leading whitespaces
        if item==nil then -- blank lines
        elseif item:contains("bin-") then bindir = item:match("bin[-](.+)"):match("^%s*(.+)") 
        elseif item:contains("lib-") then libdir = item:match("lib[-](.+)"):match("^%s*(.+)") 
        elseif item:contains("include-") then includedir = item:match("include[-](.+)"):match("^%s*(.+)") 
        else         
            premake.error("library_paths.txt doesn't have correct format. Needs to specify: bin-...\\n lib-...\\n include-... .")
            os.exit()
        end
    end
    
    if libdir==nil or includedir==nil or bindir==nil then
        premake.error("some paths are not set. Needs to specify: bin-...\\n lib-...\\n include-... .")
        os.exit()
    end
    libdirs(libdir)
    includedirs(includedir)
    bindirs(bindir)    
end

workspace "GlobalIllumination"
    language "C++"
    cppdialect "C++17"
    configurations {"Debug", "Release"}
    systemversion "10.0.16299.0"
    architecture "x86_64"
    targetdir ("build/bin/%{prj.name}/%{cfg.longname}")
    objdir ("build/obj/%{prj.name}/%{cfg.longname}")
    location "build"
    filter {"configurations:Debug"}
        defines {"DEBUG"}
        optimize "Debug"
    filter {"configurations:Release"}
        defines {"NDEBUG"}
        optimize "Full"
        symbols "Off"
    filter {"system:linux"}
        toolset "gcc" -- if you want another compiler set it here or leave erase that line (default compiler)
    filter{}

    load_library_paths()

    -- doesnt work right now. Have to manually copy things around.
    newaction {
    trigger = "deploy",
        description = "Deploys the project after it has been build using the DEPLOY target and configuration 'Release'. (Works only partially)",
        execute = function ()
            os.mkdir("deploy")
            os.copyfile("build/bin/DEPLOY/Release/DEPLOY.exe", "deploy/SVOGI.exe")
        end

    }

    function glfw_link()
        filter{"system:linux"}
            links "glfw"
        filter{"system:not linux"}
            links "glfw3"
        filter{}
    end
        
    function opengl_link()
        filter {"system:windows"}
            links {"OpenGL32"}
        filter {"system:not windows"}
            links {"GL"}
            links {"dl"} -- required by glad on non-windows systems
        filter{}
    end

    
    function glm_link()
        -- is header only lib, the header files need to be in includedir
    end

    function assimp_link()
        filter{"configurations:Debug", "system:not linux"}
            links "assimpd"
        filter{"configurations:Release or system:linux"} -- For linux always use the release libs
            links "assimp"    
        filter{}
    end
  
    function prepare_std_target()
        files {"embedded_3rdparty/glad43/**"}
        includedirs {"embedded_3rdparty/glad43/include"}

        files {"src/**","embedded_3rdparty/imgui/*"}
        includedirs {"embedded_3rdparty/imgui"}
        opengl_link()
        glfw_link()
        glm_link()
    	assimp_link()
    end
    
    project "GI"
        kind "ConsoleApp"
        prepare_std_target()
        defines {"RESOURCE_PATH=" .. path.getabsolute("resources") }

    project "DEPLOY"
        targetextension ".exe"
        kind "ConsoleApp"
        prepare_std_target()
        defines {"RESOURCE_PATH=resources"}
            
