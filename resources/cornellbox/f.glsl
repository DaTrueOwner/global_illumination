#version 430 core

in vec3 wPos;
in vec3 wNormal;
out vec4 FragColor;

uniform vec3 forward;

void main()
{
    float c = 1 - (dot(normalize(forward),wNormal) + 1) / 2.0;
    FragColor = vec4(c,c,c,1);
} 