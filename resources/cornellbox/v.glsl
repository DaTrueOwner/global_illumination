#version 430 core

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 coord;

out vec3 wPos;
out vec3 wNormal;

uniform mat4 view;
uniform mat4 proj;
//uniform mat4 model;

void main()
{
    gl_Position = (proj * view) * vec4(pos,1); 
    wPos = pos;
    wNormal = normal;
}