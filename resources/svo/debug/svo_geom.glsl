#version 430 core

#define MAX_VERTS 17

layout(points) in;
layout(line_strip, max_vertices=MAX_VERTS) out;
//layout(points, max_vertices=1) out;

uniform float half_voxel_size;
uniform mat4 mvp;
uniform mat4 vx_retransform;

const vec3 signa[MAX_VERTS] = 
{
    {-1,-1,-1},
    {1,-1,-1},
    {1,1,-1},
    {-1,1,-1},
    {-1,-1,-1},
    
    {-1,-1,1},
    {1,-1,1},
    {1,-1,-1},
    {1,1,-1},
    {1,1,1},
    {1,-1,1},
    {-1,-1,1},
    {-1,1,1},
    {1,1,1},
    {1,1,-1},
    {-1,1,-1},
    {-1,1,1}
};

void main()
{
    vec3 lPos = gl_in[0].gl_Position.xyz;
    for(int i = 0; i<MAX_VERTS;i++)
    {
        vec3 signum = signa[i];
        vec3 vx_pos = vec3(lPos.x + signum.x * half_voxel_size, lPos.y + signum.y * half_voxel_size, lPos.z + signum.z * half_voxel_size);
        gl_Position = mvp * (vx_retransform * vec4(vx_pos, 1));
        EmitVertex();
    }

    //gl_Position = mvp * vec4(lPos,1);
    EndPrimitive();
}