#version 430 core
#extension GL_ARB_shader_atomic_counter_ops : require
#define NEED_NODEDATA_DEF
#include "svo_helper.glsl"

uniform uint offset;
uniform float vx_quarter_extent;

void main()
{
    uint id = gl_VertexID + offset;     
    NodeData node = nodes[id];
    if(node.ChildPtr == 0) return;
    uint currentNodePoolHead = atomicCounterAddARB(nodePoolHead,8);
    nodes[id] = NodeData(node.CenterPosition, currentNodePoolHead,0); // position was calculated in the flag stage.  
    
    for(int i = 0; i<8; i++)
    {
        vec3 center = node.CenterPosition + octantsSignum[i] * vx_quarter_extent;
        nodes[currentNodePoolHead + i] = NodeData(center, 0,0); // Assign correct position. We will not touch it again.
    }
}