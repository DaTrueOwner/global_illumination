#version 430 core

uniform float vx_grid_dim;
uniform uint current_lvl;
uniform vec3 vx_center;
uniform mat4 vx_transformation;
uniform uint max_octree_lvl;

#define NEED_NODEDATA_DEF
#define NEED_FRAGMENTDATA_DEF
#include "svo_helper.glsl"

// TODO: Check if traversing goes wrong somehow...
void main()
{
    uint id = gl_VertexID;
    FragmentData leaf = fragments[id];
    // Transform world position to voxel grid position which starts at the origin and extents to the voxel grid size.
    vec3 leafPos = (vx_transformation * vec4(leaf.Pos,1)).xyz;
    vec3 nodeCenter = vx_center;
    uint nodeIndex = traverse(leafPos,nodeCenter,vx_grid_dim,current_lvl);
    // Here we assign the node that contains the fragment its position in the world and flag it for subdivision.
    nodes[nodeIndex] = NodeData(nodeCenter, 1,0);
}