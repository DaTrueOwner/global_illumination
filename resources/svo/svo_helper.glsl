const vec3 octantsSignum[] =
{
    {1,1,1},
    {1,1,-1},
    {1,-1,1},
    {1,-1,-1},
    {-1,1,1},
    {-1,1,-1},
    {-1,-1,1},
    {-1,-1,-1}
};

#ifdef NEED_FRAGMENTDATA_DEF
struct FragmentData
{
    vec3 Pos; 
    vec3 Albedo;
    vec3 Normal;
    vec3 Dummy; // For somewhen else
};

layout(binding = cc_bpFragmentData, std430) buffer fragmentDataSSBO
{
    FragmentData fragments[];
};

layout(binding = cc_bpFragmentDataHead, offset=0) uniform atomic_uint fragmentDataHead;


#endif

#ifdef NEED_NODEDATA_DEF
struct NodeData
{
    vec3 CenterPosition;
    uint ChildPtr; // Points to the child tile
    uint BrickPtr;
};

layout(binding = cc_bpNodePool, std430) buffer nodePoolSSBO
{
    NodeData nodes[];
};

layout(binding = cc_bpNodePoolHead) uniform atomic_uint nodePoolHead;
#endif

#ifdef NEED_NODEDATA_DEF 
#ifdef NEED_FRAGMENTDATA_DEF
uint traverse(in vec3 leafPos, inout vec3 nodeCenter, in float vx_dim, in uint to_lvl)
{
    float vx_offset = vx_dim / 2.0;
    uint nodeIndex = 0;
    NodeData node = nodes[nodeIndex];
    for(int i = 0; i<to_lvl; i++)
    {
        // we have to offset 1/4th of the parent offset to reach the child center position. 
        vx_offset = vx_offset / 2.0;
        // Traverse the tree to the current lvl
        // The current voxel has its center at c and has size s. 
        // if(leaf.c.x > c.x) -> octant = 4  (right), else octant = 0  (left)  // Most significant bit
        // if(leaf.c.y)> c.y) -> octant += 2 (front), else octant += 0 (back)  // less significant bit
        // if(leaf.c.z > c.z) -> octant += 1 (top)  , else octant += 0 (bot)   // least significant bit 
        uint octant =   4 * int(leafPos.x < nodeCenter.x) + 
                        2 * int(leafPos.y < nodeCenter.y) + 
                            int(leafPos.z < nodeCenter.z);
        
        /* This offsets the nodeCenter using a lookup table build according to the numbering of the octants. 
        The length of the offset is determined by the voxel size (Always offset by only 1/4 of the parents dimension.)*/
        nodeCenter = nodeCenter + octantsSignum[octant] * vx_offset; 

        // node.ChildPtr points to the start of the subtile. Offsetting it by "octant" calculates the correct nodeIndex. 
        nodeIndex = octant + node.ChildPtr;
        node = nodes[nodeIndex]; 
    }
    return nodeIndex;
}
#endif
#endif