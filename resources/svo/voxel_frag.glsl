#version 430 core

in vec3 vx_pos;
in vec2 fCoord;
in vec3 fNormal;

#define NEED_FRAGMENTDATA_DEF
#include "svo_helper.glsl"

uniform sampler2D tex_albedo;

void main()
{
    uint head = atomicCounterIncrement(fragmentDataHead);
    vec3 albedo = texture(tex_albedo,fCoord).rgb;
    fragments[head] = FragmentData(vx_pos, albedo,fNormal,vec3(0,0,0));
}