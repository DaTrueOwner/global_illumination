#version 430 core
layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

in vec3 vertex_normal[];
in vec2 gCoord[3];

uniform float vx_grid_dim;
uniform mat4 vx_transformation;

out vec3 vx_pos;
out vec2 fCoord;
out vec3 fNormal;

#define LOOP_BODY(swizzle) for(int i = 0; i<3; i++)\
        {\
            p[i].xyz = p[i]. swizzle ;\
        }


// Always project onto xy plane (z=1)
void main()
{

    // Triangle dominant axis selection
    // dot product is enough: sqrt(dot(v1)) {>,<,>=,<=,==} sqrt(dot(v2)) <--> dot(v1) {>,<,>=,<=,==} dot(v2) 
    /*float lx = dot(n, vec3(1,0,0)); // x-axis
    float ly = dot(n, vec3(0,1,0)); // y-axis
    float lz = dot(n, vec3(0,0,1)); // z-axis
    float dom_axis = max(lz,max(lx,ly));*/
    vec3 n = vertex_normal[0] + vertex_normal[1] + vertex_normal[2];
    float lx = abs(n.x); // x-axis
    float ly = abs(n.y); // y-axis
    float lz = abs(n.z); // z-axis
    uint dom_axis = 2;
    if(lx > ly && lx > lz) dom_axis = 0;
    if(ly > lx && ly > lz) dom_axis = 1;
    vec3 p[3];
    // Copy
    for(int i = 0; i<3; i++) 
    {
        p[i] = (vx_transformation * gl_in[i].gl_Position).xyz; // To voxel space
    }
    // project onto x=1 -> we want to project on z=1
    if (dom_axis == 0) LOOP_BODY(zyx)
    // project onto y=1 -> we want z=1
    else if(dom_axis == 1) LOOP_BODY(xzy)

    for(int i = 0; i<3; i++)
    {
        // Object is in voxel grid space -> [0;vx_grid_dim]^3. Transformation of voxel grid space to clip space ([-1;1]^3): 
        // [0/vx_grid_dim * 2 - 1;vx_grid_dim / vx_grid_dim * 2 - 1] == [-1;1] 
        p[i].xy = (p[i].xy / vx_grid_dim)  * 2.0 - vec2(1.0); 
        p[i].z = 1; 
        gl_Position = vec4(p[i],1);
        vx_pos = gl_in[i].gl_Position.xyz;
        fCoord = gCoord[i];
        fNormal = vertex_normal[i];
        EmitVertex();
    }
    EndPrimitive();
}
