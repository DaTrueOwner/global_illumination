#include "App.h"
#include <GLFW/glfw3.h>
#include <cstdio>

App::App() : window(*glfwGetCurrentContext())
{
}

void App::SetTimeForThisFrame(double deltaTime)
{
	this->deltaTime = deltaTime / 1000.; // Convert to seconds
}

float App::GetSizeX() const
{
	int sx,sy;
	glfwGetWindowSize(&window, &sx, &sy);
	return sx;
}

float App::GetSizeY() const
{
	int sx, sy;
	glfwGetWindowSize(&window, &sx, &sy);
	return sy;
}

double App::GetMousePosX() const
{
	double px, py;
	glfwGetCursorPos(&window, &px, &py);
	return px;
}


double App::GetMousePosY() const
{
	double px, py;
	glfwGetCursorPos(&window, &px, &py);
	return py;
}

void App::Update()
{
}
