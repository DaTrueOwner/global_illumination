#pragma once

class App
{
public:
	App();
	void SetTimeForThisFrame(double deltaTime);
	virtual void HandleResize(float width, float height) {}
	float GetSizeX() const;
	float GetSizeY() const;
	double GetMousePosX() const;
	double GetMousePosY() const;

	virtual void Update();

	virtual ~App() {}

protected:
	double deltaTime = 0;
	class GLFWwindow& window;
};
