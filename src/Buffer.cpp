#include "Buffer.h"
#include "glad/glad.h"


Buffer::Buffer(GLenum bufferType, const BufferDataVector& data, GLenum usagePattern)
{
	glGenBuffers(1, &id);
	glBindBuffer(bufferType, id);
	glBufferData(bufferType, data.size, data.data_ptr, usagePattern);
	this->bufferType = bufferType;
}

Buffer::Buffer(GLenum bufferType, GLuint data, GLenum usagePattern)
	:Buffer(bufferType, BufferDataVector{ &data, sizeof(GLuint) }, usagePattern)
{}

void Buffer::move(Buffer&& other)
{
	this->id = other.id;
	this->bufferType = other.bufferType;
	other.id = 0;
}

