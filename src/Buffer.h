#pragma once
#include "glad/glad.h"
#include <stdexcept>
#include "NonCopyable.h"
#include <cstring>

struct BufferDataVector
{
	void* data_ptr;
	size_t size;
};


class Buffer
{
public:
	MAKE_NO_COPY(Buffer)
	MAKE_SIMPLE_MOVE(Buffer)
	Buffer(){}
	Buffer(GLenum bufferType, const BufferDataVector& data, GLenum usagePattern = GL_STATIC_DRAW);
	Buffer(GLenum bufferType, GLuint data, GLenum usagePattern = GL_STATIC_DRAW);
	
	void Bind() const
	{
		glBindBuffer(bufferType, id);
	}
	void Bind(int location) const
	{
		glBindBufferBase(bufferType, location, id);
	}

	void BindAsType(GLenum bufferType) const
	{
		glBindBuffer(bufferType, id);
	}

	void BindAsType(GLenum bufferType, int location) const
	{
		glBindBufferBase(bufferType, location, id);
	}

	void SetData(const BufferDataVector& data)
	{
		void* ptr = glMapBufferRange(bufferType, 0, data.size, GL_MAP_WRITE_BIT);
		memcpy(ptr,data.data_ptr,data.size);
		glUnmapBuffer(bufferType);
	}

	void SetType(GLenum type) { bufferType = type; }
	GLenum GetType() const { return bufferType; }

	~Buffer()
	{
		glDeleteBuffers(1, &id);
	}

private:
	GLuint id = 0;
	GLenum bufferType = 0;
	void move(Buffer&& other);


};