#include "CornellBoxApp.h"
#include "Loader.h"
#include "imgui.h"
#include "glad/glad.h"
#include <GLFW/glfw3.h>
#include "Model.h"
#include <cstring>


#define SPEED (5.f)
#define ROTATION_SPEED (4.f)


CornellBoxApp::CornellBoxApp():
		s(RES("/cornellbox/v.glsl"),
			RES("/cornellbox/f.glsl"))
		
{

	std::string path = RES("/cornellbox/cornellbox.fbx");
	std::vector<StaticMesh> meshes = Loader::LoadMeshes(Loader::PiPath(path),minScene,maxScene);
    models.reserve(meshes.size());
    for(StaticMesh& mesh : meshes)
    {
        float vals[3];
        if(mesh.GetName() == "wall_right") 
        {
			float g[3] = { 0,1,0 };
            std::memcpy(vals,g,3);
        }
        
        else if(mesh.GetName() == "wall_left")
        {
			float r[3] = { 1,0,0 };
			std::memcpy(vals, r, 3);
        }

        auto t = std::make_shared<Texture>(1,1,GL_RGB,GL_RGB,GL_UNSIGNED_BYTE,vals);
        Material m;
		m.SetShader(svob.GetEntryShaderId());
        m.SetAlbedo(t);
		models.push_back(Model{ std::move(mesh),m });
    }
    
	cam.SetEyePos({ 0,5,0});
	cam.LookAt({ 0,0, 0 });
	cam.SetPerspectiveProj(45.0f, GetSizeX() / GetSizeY(), 0.01f, 1000);
	svob.PrebuildSVO(models,GetSizeX(), GetSizeY(), minScene,maxScene);
}

void CornellBoxApp::Update()
{
	handle_movement();
	svob.DebugSVO(cam.GetProjMatrix() * cam.GetViewMatrix());
	//svob.DebugFragmentList(cam.GetProjMatrix() * cam.GetViewMatrix());
	
	/*s.Use();
	s.Set("proj", cam.GetProjMatrix());
	s.Set("view", cam.GetViewMatrix());
	s.Set("forward", cam.GetForward());

	for(const auto& mesh : meshes)
	{
		mesh.Draw();
	}*/
}

void CornellBoxApp::handle_movement()
{
	if (glfwGetKey(&window, GLFW_KEY_E) == GLFW_PRESS) cam.Move({ 0,0,SPEED * deltaTime });
	if (glfwGetKey(&window, GLFW_KEY_Q) == GLFW_PRESS) cam.Move({ 0,0,-SPEED * deltaTime });
	if (glfwGetKey(&window, GLFW_KEY_A) == GLFW_PRESS) cam.Move({ -SPEED * deltaTime,0,0 });
	if (glfwGetKey(&window, GLFW_KEY_D) == GLFW_PRESS) cam.Move({ SPEED * deltaTime,0,0 });
	if (glfwGetKey(&window, GLFW_KEY_S) == GLFW_PRESS) cam.Move({ 0,-SPEED * deltaTime,0 });
	if (glfwGetKey(&window, GLFW_KEY_W) == GLFW_PRESS) cam.Move({ 0,SPEED * deltaTime,0 });

	if (glfwGetMouseButton(&window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS) {
		int deltaX = oldX - GetMousePosX();
		int deltaY = oldY - GetMousePosY();

		cam.RotateEye(-deltaX * deltaTime * ROTATION_SPEED, deltaY * deltaTime * ROTATION_SPEED);
	}

	oldX = GetMousePosX();
	oldY = GetMousePosY();
}

void CornellBoxApp::HandleResize(float width, float height)
{
	cam.SetPerspectiveProj(45.0f, width / height, 0.01f, 1000);
	//cam.SetOrthoProj(-1.0f, 1.0f, -1.0f, 1.0f, .1f, 100.f);
}
