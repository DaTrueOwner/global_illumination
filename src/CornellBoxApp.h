#pragma once
#include "App.h"
#include "Camera.h"
#include "Shader.h"
#include "SVOBuilder.h"

class Model;

class CornellBoxApp : public App
{
public:
	CornellBoxApp();
	void Update() override;
	void HandleResize(float width, float height) override;

private:
	std::vector<Model> models;
	SVOBuilder svob;
	Camera cam;
	Shader s;
	int oldX;
	int oldY;
	glm::vec3 minScene, maxScene;

	void handle_movement();
};
