#pragma once

#include <cstdint>

using u32 = uint32_t;
using u64 = uint64_t;
using i32 = int32_t;
using i64 = int64_t;
using byte = uint8_t;