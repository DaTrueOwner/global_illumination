#pragma once
#include "OctreeInterface.h"

class LightingEngine
{
	LightingEngine() = default;
	void PrebuildLighting(OctreeInterface&& octree);
private:
	OctreeInterface octree;
	
	Texture normalBricks;
	Texture colorBricks;
	Texture radianceBricks;
};
