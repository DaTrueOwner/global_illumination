#include "Loader.h"
#include "glm/vec3.hpp"
#include "assimp/Importer.hpp"
#include "assimp/postprocess.h"
#include "assimp/mesh.h"
#include "assimp/scene.h"
#include <stdexcept>
#include "glm/trigonometric.hpp"

void createVertices(std::vector<Vertex>& p_vertices, aiMesh* p_assMesh)
{
	for (u32 i = 0; i < p_assMesh->mNumVertices; i++)
	{
		Vertex v;

		aiVector3D assPos = p_assMesh->mVertices[i];

		glm::vec3 pos{ assPos.x, assPos.y, assPos.z }; // Somehow assimp imports it 90 degrees rotated...
		v.Position = pos;


		aiVector3D assNormal = p_assMesh->mNormals[i];
		glm::vec3 normal{ assNormal.x, assNormal.y, assNormal.z }; // Flip it here too.
		v.Normal = normal;

		p_vertices.push_back(v);

	}
}
void createIndices(std::vector<u32>& p_indices, aiMesh* p_assMesh)
{
	for (u32 i = 0; i < p_assMesh->mNumFaces; i++)
	{
		aiFace& face = p_assMesh->mFaces[i];

		for (u32 j = 0; j < face.mNumIndices; j++)
		{
			p_indices.push_back(face.mIndices[j]);
		}
	}
}

const aiScene* loadScene(const char* p_filename, Assimp::Importer& p_importer, u32 p_steps = aiProcessPreset_TargetRealtime_MaxQuality)
{
	const aiScene* scene = p_importer.ReadFile(p_filename, p_steps);
	if (!scene) throw std::runtime_error("Couldn't load or read file: " + std::string(p_importer.GetErrorString()));
	if (!scene->HasMeshes()) throw std::runtime_error("Specified file contains no meshes.");
	return scene;
}

void getMinMax(const std::vector<Vertex>& vertices, glm::vec3& min, glm::vec3& max)
{
	for (const Vertex& v : vertices)
	{
		if (v.Position.x > max.x) max.x = v.Position.x;
		if (v.Position.x < min.x) min.x = v.Position.x;
		if (v.Position.y > max.y) max.y = v.Position.y;
		if (v.Position.y < min.y) min.y = v.Position.y;
		if (v.Position.z > max.z) max.z = v.Position.z;
		if (v.Position.z < min.z) min.z = v.Position.z;
	}
}

std::vector<StaticMesh> Loader::LoadMeshes(const std::string& filename, glm::vec3& min, glm::vec3& max)
{
	Assimp::Importer importer;
	const aiScene* scene = loadScene(filename.c_str(), importer, aiProcessPreset_TargetRealtime_MaxQuality | aiProcess_PreTransformVertices);
	std::vector<StaticMesh> out;

	max.x = std::numeric_limits<float>::min(), max.y = std::numeric_limits<float>::min(), max.z = std::numeric_limits<float>::min(),
		min.x = std::numeric_limits<float>::max(), min.y = std::numeric_limits<float>::max(), min.z = std::numeric_limits<float>::max();

	for (u32 i = 0; i<scene->mNumMeshes; i++)
	{
		std::vector<Vertex> vertices;
		std::vector<u32> indices;
		createVertices(vertices, scene->mMeshes[i]);
		getMinMax(vertices,min,max);
		createIndices(indices, scene->mMeshes[i]);
        std::string name = scene->mMeshes[i]->mName.C_Str();
		out.push_back(StaticMesh(vertices, indices, name));
	}

	return out;
}


std::string Loader::PiPath(const std::string& path)
{
	return path; // For now just return it.
}



