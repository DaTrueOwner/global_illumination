#pragma once
#include <string>
#include "StaticMesh.h"

namespace Loader
{
	/// Make path platform independent.
	std::string PiPath(const std::string& path);
	std::vector<StaticMesh> LoadMeshes(const std::string& filename, glm::vec3& min, glm::vec3& max);
}