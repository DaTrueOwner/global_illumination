#include "Material.h"
#include "Shader.h"
#include <glm/gtc/type_ptr.hpp>
#include "Texture.h"

const float white[] = {1,1,1};

std::shared_ptr<Texture> Material::stdAlbedoTex = nullptr;

Material::Material()
{
    this->albedoTex = stdAlbedoTex;
}

void Material::SetShader(GLuint id) 
{
	programId = id;
	this->albedoTexLocation = glGetUniformLocation(programId, "tex_albedo");
}

void Material::SetAlbedo(const std::shared_ptr<Texture>& texAlbedo)
{
	this->albedoTex = texAlbedo;
}

void Material::Use() const
{
    GLuint activeTexture = GL_TEXTURE0; 
	if(albedoTexLocation != -1) 
    {
        glUniform1i(albedoTexLocation,activeTexture - GL_TEXTURE0);
        glActiveTexture(activeTexture);
		albedoTex->Bind();
        activeTexture++;
    }
    
}

void Material::Init()
{
	stdAlbedoTex = std::make_shared<Texture>(1, 1, GL_RGB, GL_RGB, GL_UNSIGNED_BYTE, &white);
}
