#pragma once
#include "glad/glad.h"
#include <string>
#include <memory>



// TODO: Make sure that if shader is destroy, all corresponding instances are also destroyed / marked invalid.

class Texture;

/*
 * Convenience class when dealing with materials that are attached to an object (like a mesh for example)
 */
class Material
{
public:
	Material();
	void SetShader(GLuint id);
	void SetAlbedo(const std::shared_ptr<Texture>& tex);
	void Use() const;
	static void Init();

private:

	GLuint programId = 0;
	std::shared_ptr<Texture> albedoTex = nullptr;
	GLuint albedoTexLocation = 0;
    static std::shared_ptr<Texture> stdAlbedoTex;
};
