#pragma once
#include "Buffer.h"
#include <vector>
#include "glm/mat4x4.hpp"
#include "glm/vec3.hpp"
#include "NonCopyable.h"
#include "Texture.h"

// This structure is meant as an interface to gather all necessary information to build a LightingEngine from a SVOBuilder
struct OctreeInterface
{
	OctreeInterface() = default;
	MAKE_NO_COPY(OctreeInterface)
	MAKE_DEFAULT_MOVE(OctreeInterface)
	float VXGridDimension;
	glm::vec3 VXCenter;
	glm::mat4 VXTransformation;
	glm::mat4 VXRetransformation;
	std::vector<GLuint> AllocatedNodes;
	Buffer NodePool;
	Buffer FragmentList;
	
	// Releases every GPU resource to free memory. This calls the destructor, so FragmentList is in an invalid state after a call to this method.
	void DisposeFragmentList()
	{
		FragmentList.~Buffer();
	}

};