#include "SVOBuilder.h"
#include "Definitions.h"
#include "glm/vec3.hpp"
#include "Camera.h"
#include <array>
#include <GLFW/glfw3.h>
#include "Texture.h"

// Things that need to be evaluated at compile time
constexpr Std430 FragmentData[] = { Std430::Gvec3 };
constexpr Std430 NodeData[] = { Std430::Gvec3, Std430::Guint, Std430::Guint };
constexpr GLuint SizeOfFragmentData = LayoutStd430Struct(FragmentData);
constexpr GLuint SizeOfNodeData = LayoutStd430Struct(NodeData);

struct node_data
{
	glm::vec3 center = { 0, 0, 0 }; GLuint child_ptr = 0; GLuint brick_ptr = 0; byte padding[10]; // 10 byte padding to align it to the rules of std430 layout.
};
static_assert(sizeof(node_data) == SizeOfNodeData);

constexpr GLuint BrickSize = 3 * 3 * 3;

SVOBuilder::SVOBuilder(int vxGridDimension_) : vxGridDimension(vxGridDimension_),
fragmentListHead(GL_ATOMIC_COUNTER_BUFFER, 0),
nodePoolHead(GL_ATOMIC_COUNTER_BUFFER, 16),
voxelShader(RES("/svo/voxel_vert.glsl"), RES("/svo/voxel_geom.glsl"), RES("/svo/voxel_frag.glsl"), mapping),
//indirectBuffer(GL_SHADER_STORAGE_BUFFER, {nullptr,3*sizeof(GLuint)}),
nodeFlagShader(RES("/svo/nodeflag_vert.glsl"), RES("/svo/empty_frag.glsl"),mapping),
//nodeFlagSetter(RES("/svo/nodeflag_setter.glsl")),
nodeCreateShader(RES("/svo/nodecreate_vert.glsl"), RES("/svo/empty_frag.glsl"),mapping),
debugFragmentListShader(RES("/svo/debug/fragmentList_vert.glsl"), RES("/svo/debug/fragmentList_frag.glsl")),
debugSVOShader(RES("/svo/debug/svo_vert.glsl"), RES("/svo/debug/svo_geom.glsl"), RES("/svo/debug/svo_frag.glsl"))
//nodeInitSetter(RES("/svo/nodeinit_setter.glsl")),
//nodeInitShader(RES("/svo/nodeInitShader_comp.glsl"))
{
	if (!GLAD_GL_ARB_shader_atomic_counter_ops) throw std::runtime_error("Couldn't load necessary extension: ARB_shader_atomic_counter_ops");
	if (GLAD_GL_NV_conservative_raster)
	{
		conservative_cap = GL_CONSERVATIVE_RASTERIZATION_NV;
	}

	else if (GLAD_GL_INTEL_conservative_rasterization)
	{
		conservative_cap = GL_CONSERVATIVE_RASTERIZATION_INTEL;
	}

	else printf("WARNING: No hardware support for conservative rasterization!\n");

	int max_size;
	glGetIntegerv(GL_MAX_SHADER_STORAGE_BLOCK_SIZE, &max_size);

	// TODO check for max memory size..
	maxElemAmount = vxGridDimension * vxGridDimension * vxGridDimension;

	size_t bytesFragmentList = maxElemAmount * SizeOfFragmentData;
	size_t bytesSVO = maxElemAmount * SizeOfNodeData;
	if (bytesSVO >= max_size || bytesFragmentList >= max_size) throw std::runtime_error("ERROR: GPU doesn't support the allocation of more than " + std::to_string(max_size) + " bytes which is needed right now.");
	fragmentList = Buffer(GL_SHADER_STORAGE_BUFFER, { nullptr, bytesFragmentList }); // Enough place for 1M elements.
	nodePool = Buffer(GL_SHADER_STORAGE_BUFFER, { nullptr,bytesSVO }); // Allocate only.

}

// The variable has to be bound.
GLuint getAtomicCounterVar()
{
	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
	GLuint out = *(GLuint*)glMapBufferRange(GL_ATOMIC_COUNTER_BUFFER, 0, sizeof(GLuint), GL_MAP_READ_BIT);
	glUnmapBuffer(GL_ATOMIC_COUNTER_BUFFER);
	return out;
}

void SVOBuilder::DebugFragmentList(const glm::mat4& mvp) const
{
	VertexArray vao;
	vao.Bind();
	fragmentList.BindAsType(GL_ARRAY_BUFFER);
	glMemoryBarrier(GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, SizeOfFragmentData, nullptr);
	debugFragmentListShader.Use();
	debugFragmentListShader.Set("mvp", mvp);
	glDrawArrays(GL_POINTS, 0, staticSceneFragListCount);
	glBindVertexArray(0);
}


void SVOBuilder::DebugSVO(const glm::mat4& vp) const
{
	VertexArray vao;
	vao.Bind();
	glMemoryBarrier(GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT);
	nodePool.BindAsType(GL_ARRAY_BUFFER);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, SizeOfNodeData, nullptr);
	debugSVOShader.Use();
	debugSVOShader.Set("mvp", vp);
	debugSVOShader.Set("vx_retransform", vxRetransformation);
	//debugSVOShader.Set()
	int n = 0;
	float vxHalfVoxelSize = vxGridDimension;
	for (size_t i = 0; i<allocatedNodes.size(); i++)
	{
		vxHalfVoxelSize /= 2.0;
		GLuint k = allocatedNodes[i];
		debugSVOShader.Set("half_voxel_size", vxHalfVoxelSize);
		if(i==allocatedNodes.size()-1) glDrawArrays(GL_POINTS, n, i == 0 ? 1 : k); // Dont render disfunct nodes at the start
		n += k;
	}
	glBindVertexArray(0);
}

void SVOBuilder::PrebuildSVO(const std::vector<Model>& models, float framebufferSizeX, float framebufferSizeY, const glm::vec3 & min, const glm::vec3 & max)
{

	// The voxel grid size has to be big enough to encompass the entire static scene.
	// Therefore we need to get from modelspace to voxel space by translating and scaling the scene to fit into the grid.
	// After translating the local center point to the voxel center point (0,0,0), we scale it so that the absolute max value == voxelGridDimension.
	// => scale = scale to unit cube (divide by max value or multiply by 1/max value -> scale to voxel grid (multiply by voxel grid dim) 
	glm::vec3 modelExtents = abs(max - min);

	float vxScale = 1.f / std::max({ modelExtents.x,modelExtents.y,modelExtents.z }) * vxGridDimension;
	vxCenter = glm::vec3(vxGridDimension / 2.0f);
	vxTransformation = glm::scale(glm::mat4(1.0f), glm::vec3(vxScale, vxScale, vxScale)) * glm::translate(glm::mat4(1.0), -min);
	vxRetransformation = glm::inverse(vxTransformation);

	float vxOffset = vxGridDimension / 4.0f;

	node_data root[] =
	{
		{ vxCenter,8,0},{},{},{},{},{},{},{},
	{ { vxCenter.x + vxOffset,vxCenter.y + vxOffset,vxCenter.z + vxOffset }, 0,0 },
	{ { vxCenter.x + vxOffset,vxCenter.y + vxOffset,vxCenter.z - vxOffset }, 0,0 },
	{ { vxCenter.x + vxOffset,vxCenter.y - vxOffset,vxCenter.z + vxOffset }, 0,0 },
	{ { vxCenter.x + vxOffset,vxCenter.y - vxOffset,vxCenter.z - vxOffset }, 0,0 },
	{ { vxCenter.x - vxOffset,vxCenter.y + vxOffset,vxCenter.z + vxOffset }, 0,0 },
	{ { vxCenter.x - vxOffset,vxCenter.y + vxOffset,vxCenter.z - vxOffset }, 0,0 },
	{ { vxCenter.x - vxOffset,vxCenter.y - vxOffset,vxCenter.z + vxOffset }, 0,0 },
	{ { vxCenter.x - vxOffset,vxCenter.y - vxOffset,vxCenter.z - vxOffset }, 0,0 },
	};

	nodePool.SetData({ &root,SizeOfNodeData * (sizeof root / sizeof root[0]) }); // Initialize memory of buffer

	fragmentList.Bind(0);
	fragmentListHead.Bind(0);

	glViewport(0, 0, vxGridDimension, vxGridDimension);
	//glfwSetWindowSize(window, vxGridDimension, vxGridDimension);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_MULTISAMPLE);
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

	glEnable(conservative_cap);
	voxelShader.Use();
	voxelShader.Set("vx_grid_dim", vxGridDimension);
	voxelShader.Set("vx_transformation", vxTransformation);
	for (const auto& model : models) model.Draw();
	glDisable(conservative_cap);

	staticSceneFragListCount = getAtomicCounterVar();
	assert(staticSceneFragListCount < maxElemAmount);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	nodePool.Bind(1);
	//indirectBuffer.Bind();
	nodePoolHead.Bind(1);
	nodePoolHead.Bind(); // Also bind it as standard because staticSceneFragListCount won't be altered from here.
						 /*
						 * - Iterate over fragment list and traverse the octree down to the current level by calculating the correct subnode and chasing the pointer -> Flag nodes that need to be subdivided.
						 * - Next step is to create new octants spawned from the flagged nodes. Here iterate over each node of the octants which the flagged nodes belong to.
						 *	 - 1) Assume that the nodes of the current level lie next to each other in a linear memory layout.
						 *	 - 2) Assume that only the octants of the flagged nodes are allocated and "come first" in memory without any holes.
						 *	 - 3) Assume that n is the offset to reach the nodes of the current level.
						 *	 - 4) Assume that k is the (# of octants which may contain flagged nodes * 8) at the current level.
						 *	- Dispatch a compute shader whose total work size (here realised with a vertex shader) == k. (4)
						 *	- In the shader the first flagged node can appear only at index n (3,1). Thus we get the ID of the current thread (which is [0,k] ) and offset it by n. This calculation is used as an index into the node array. In this way all possible flagged nodes are considered. (2,3,4)
						 *	- If the node is flagged we atomically increment the nodePoolHead which effectively means allocating an octant of size 8 at index nodePoolHead before the increment operation.
						 *	- If the node isn't flagged, it has no children => Return.
						 *	- After this operation each compute thread has allocated a continuous chunk of memory (fullfills 1) for the nodes of the next level and created links between the nodes and corresponding subnodes.
						 *	- Also by exiting the thread when a node isn't flagged no memory is allocated for non existant child nodes -> in the next iteration these nodes will obviously not receive a flag even if they were allocated (fullfills 2). Also note that the nodes of the next octree level are allocated right next to the ones of the    current lvl
						 *	- Next we calculate the new k. nodePoolHead now points to the next free memory block. Thus nodePoolHead - (n+k) yields the new k, that is the count of freshly allocated, ready to be flagged nodes. (fullfills 4)
						 *	- New n = n + k (fullfills 3)
						 *
						 */

						 // The root node and its seven disfunct following nodes are already allocated and valid. the child ptr of the root node points to index 8. The eight child nodes are only allocated and have to be flagged for further division.
	GLuint n = 8; // 8 is the offset to reach the nodes of the second lvl.
	GLuint k = 8;
	GLuint maxOctreeLvl = std::log(std::pow(vxGridDimension, 3)) / std::log(8);
	allocatedNodes.reserve(maxOctreeLvl);
	allocatedNodes.push_back(k); // The first 8 nodes
	zeroVao.Bind();
	// TODO: assign the work on the GPU.
	for (GLuint i = 1; i < maxOctreeLvl-1; i++)
	{
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
		vxOffset /= 2.0f;
		// 1. Flag the nodes.
		nodeFlagShader.Use(); // Uses the nodePool (binding=1), fragmentList (binding=0). Bound to index outside the loop. -> Needs to sync: nodePool (synced at the end of the loop), fragmentList (synced before the loop)
		nodeFlagShader.Set("vx_grid_dim", vxGridDimension);
		nodeFlagShader.Set("current_lvl", i);
		nodeFlagShader.Set("vx_transformation", vxTransformation);
		nodeFlagShader.Set("vx_center", vxCenter);
		glDrawArrays(GL_POINTS, 0, staticSceneFragListCount); // At level 1 we always subdivide the root node. There is no point in launching a mass of vertex shader invocations.

		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT); // Make sure nodePool writes (flagging) are synced.

														// 2. Subdivide and allocate the nodes.
		nodeCreateShader.Use();
		nodeCreateShader.Set("offset", n);
		nodeCreateShader.Set("vx_quarter_extent", vxOffset);
		glDrawArrays(GL_POINTS, 0, k);
		allocatedNodes.push_back(k);
		// Calc the new k
		int oldK = k;
		GLuint atomicCounterVar = getAtomicCounterVar();
		k = atomicCounterVar - (n + k);
		// Calc the new n
		n = n + oldK;
	}
	allocatedNodes.push_back(k);
	// TODO: Continue here....



	for (int i = 0; i < allocatedNodes.size(); i++)
	{
		GLuint k = allocatedNodes[i];
		printf("i==%i: k=%i\n", i, k);
	}


	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glViewport(0, 0, framebufferSizeX, framebufferSizeY);
	glBindVertexArray(0);
}

GLuint SVOBuilder::GetEntryShaderId() const
{
	return voxelShader.GetId();
}
