#pragma once
#include "glad/glad.h"
#include <vector>
#include "Shader.h"
#include "Buffer.h"
#include "Texture.h"
#include "VertexArray.h"
#include "Model.h"


class StaticMesh;
class SVOBuilder
{
public:
	SVOBuilder(int vxGridDimension_ = 128);
	void DebugFragmentList(const glm::mat4& proj) const;
	void DebugSVO(const glm::mat4 & mvp) const;
	void PrebuildSVO(const std::vector<Model>& models, float viewportSaveX, float viewportSaveY, const glm::vec3& min, const glm::vec3& max);
	GLuint GetEntryShaderId() const;
	void DisposeFragmentList();

private:
	float vxGridDimension;
	glm::vec3 vxCenter;
	glm::mat4 vxTransformation; // Transforms into grid space
	glm::mat4 vxRetransformation; // Retransforms back into world space

	// Binding locations for use in shader and cpu code
	GLuint bpFragmentDataHead = 0;
	GLuint bpFragmentData = 0;
	GLuint bpNodePoolHead = 1;
	GLuint bpNodePool = 1;
	// Mappings used in the shader (eg. cc_bpFragmentDataHead)
	std::vector<ConstantReplaceMapping> mapping = {{"bpFragmentDataHead",std::to_string(bpFragmentDataHead)}, {"bpFragmentData",std::to_string(bpFragmentData) },{"bpNodePoolHead",std::to_string(bpNodePoolHead)},{"bpNodePool",std::to_string(bpNodePool)}};

	size_t maxElemAmount;
	GLuint conservative_cap = 0;
	Buffer fragmentListHead;
	Buffer fragmentList;
	Buffer nodePool;
	Buffer nodePoolHead;
	Shader voxelShader;
	Shader nodeFlagShader;
	Shader nodeCreateShader;
	std::vector<GLuint> allocatedNodes;


	// BrickPools
	Texture radianceBrickPool;
	Texture normalBrickPool;
	Texture albedoBrickPool;

	// Debug
	Shader debugFragmentListShader;
	GLuint staticSceneFragListCount;

	Shader debugSVOShader;

	VertexArray zeroVao;
};