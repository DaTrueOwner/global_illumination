#include "SceneChooser.h"
#include "CornellBoxApp.h"
#include "imgui.h"
#include "GLFW/glfw3.h"

SceneChooser::SceneChooser() : window(*glfwGetCurrentContext())
{
}

void SceneChooser::Choose(std::unique_ptr<App>& app)
{
	ImGui::Begin("SceneChooser");
	const char* const items[] = { "Sponza Scene", "Landing Page" };
	ImGui::ListBox("Choose Scene", &current_item, items, sizeof(items) / sizeof(const char*));
	ImGui::Separator();
	bool newScene = ImGui::Button("Ok");
	ImGui::End();

	if (newScene)
	{
		if (strcmp(items[current_item], "Sponza Scene") == 0) {
			app = std::make_unique<CornellBoxApp>();
		}
		else app = std::make_unique<App>(); // Landing page
	}
}
 

