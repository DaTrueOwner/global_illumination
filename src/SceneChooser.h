#pragma once
#include "App.h"
#include <memory>

namespace sf
{
	class Window;
}

class SceneChooser
{
public:
	SceneChooser();
	void Choose(std::unique_ptr<App>& app);
private:
	int current_item=0;
	const class GLFWwindow& window;

};
