#include "Shader.h"
#include <vector>
#include <fstream>
#include <sstream>
#include <glm/gtc/type_ptr.hpp>
#include <regex>
#include <unordered_set>

struct ShaderSource
{
	std::string Src;
	GLenum Type;
	std::string Location;
};


void replaceConstants(std::string& src, const std::vector<ConstantReplaceMapping>& crMap)
{
	for (const auto& cr : crMap)
	{
		src = std::regex_replace(src, std::regex("cc_" + cr.Constant), cr.ReplaceWith);
	}
}

std::string preprocessIncludes(const std::string& path, const std::string& absLocation, std::unordered_set<std::string>& alreadyIncluded )
{
	static const std::regex re("^[ ]*#[ ]*include[ ]+[\"](.*)[\"].*");
	std::stringstream src; 
	std::ifstream srcStream(path);
	if (!srcStream) throw std::runtime_error("Cannot open file: " + path);
	src << srcStream.rdbuf();
	std::smatch matches;
	std::string out;
	std::string line;
	int counter = 1;
	while(std::getline(src,line))
	{
		counter++;
		if(std::regex_search(line,matches,re))
		{
			std::string includeFile = absLocation + "/" + std::string(matches[1]);
			if (alreadyIncluded.find(includeFile) == alreadyIncluded.end())
			{
				out += "#line 1\n" + preprocessIncludes(includeFile, absLocation, alreadyIncluded) + "#line "+std::to_string(counter)+"\n";
				alreadyIncluded.insert(includeFile);
				continue;
			}
		}
		out += line+"\n";
	}
	out.shrink_to_fit();
	return out;
}

// Taken from my github acccount https://github.com/DaOnlyOwner/tools3d/blob/master/src/VBO.cpp with slight modification
inline GLuint compileShader(std::string& code, GLenum p_type, const std::string& location, const std::vector<ConstantReplaceMapping>& crMap)
{
	replaceConstants(code,crMap);
	const GLchar* ccode = code.c_str();
	GLint success;
	GLuint shaderId = glCreateShader(p_type);
	glShaderSource(shaderId, 1, &ccode, nullptr);
	glCompileShader(shaderId);
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		char log[10000];
		glGetShaderInfoLog(shaderId, 10000, nullptr, log);
		throw std::runtime_error("Shader at: " + location + " didn't compile successfully:\n" + std::string(log));
	}

	return shaderId;
}

// Taken from my github acccount https://github.com/DaOnlyOwner/tools3d/blob/master/src/VBO.cpp with slight modification
inline GLuint createProgram(std::vector<ShaderSource>& srcs, const std::vector<ConstantReplaceMapping>& crMap)
{
	GLuint programOut = glCreateProgram();

	std::vector<GLuint> saveIds; saveIds.reserve(srcs.size());

	for (unsigned int i = 0; i < srcs.size(); i++)
	{
		saveIds.push_back(compileShader(srcs[i].Src, srcs[i].Type,srcs[i].Location, crMap));
		glAttachShader(programOut, saveIds.back());
	}

	glLinkProgram(programOut);
	int success;
	glGetProgramiv(programOut, GL_LINK_STATUS, &success);
	if (!success)
	{
		char log[10000];
		glGetProgramInfoLog(programOut, 10000, nullptr, log);
		throw std::runtime_error("Error: Linking of shaders failed:\n" + std::string(log));
	}

	for (GLuint saveId : saveIds)
	{
		glDetachShader(programOut, saveId);
		glDeleteShader(saveId);
	}
	return programOut;
}

ShaderSource GenShaderSource(const std::string& path, GLenum shaderType)
{
	int localPath = path.find_last_of('/');
	std::unordered_set<std::string> startSet;
	startSet.insert(path);
	std::string src = preprocessIncludes(path, path.substr(0,localPath),startSet);
	return { src,shaderType, path };
}


Shader::Shader(const std::string& vertexPath, const std::string& fragmentPath, const std::vector<ConstantReplaceMapping>& crMap)
{
	std::vector<ShaderSource> srcs = { GenShaderSource(vertexPath,GL_VERTEX_SHADER), GenShaderSource(fragmentPath,GL_FRAGMENT_SHADER) };
	id = createProgram(srcs,crMap);
}

// TODO: Refactor
Shader::Shader(const std::string& vertexPath, const std::string& geomPath, const std::string& fragmentPath, const std::vector<ConstantReplaceMapping>& crMap)
{
	std::vector<ShaderSource> srcs = { GenShaderSource(vertexPath,GL_VERTEX_SHADER), GenShaderSource(geomPath, GL_GEOMETRY_SHADER), GenShaderSource(fragmentPath,GL_FRAGMENT_SHADER)};
	id = createProgram(srcs, crMap);
}

Shader::Shader(const std::string& compPath, const std::vector<ConstantReplaceMapping>& crMap)
{
	std::vector<ShaderSource> srcs = { GenShaderSource(compPath,GL_COMPUTE_SHADER) };
	id = createProgram(srcs, crMap);
}

void Shader::Set(const std::string& key, GLfloat value) const
{
	GLuint location = glGetUniformLocation(id, key.c_str());
	glUniform1f(location, value);
}

void Shader::Set(const std::string& key, GLuint value) const
{
	GLuint location = glGetUniformLocation(id, key.c_str());
	glUniform1ui(location, value);
}

void Shader::Set(const std::string& key, GLint value) const
{
	GLuint location = glGetUniformLocation(id, key.c_str());
	glUniform1i(location, value);
}

void Shader::Set(const std::string& key, const glm::mat4& mat) const
{
	GLuint location = glGetUniformLocation(id, key.c_str());
	glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(mat));
}

void Shader::Set(const std::string& key, const glm::mat3& mat) const
{
	GLuint location = glGetUniformLocation(id, key.c_str());
	glUniformMatrix3fv(location, 1, GL_FALSE, glm::value_ptr(mat));
}

void Shader::Set(const std::string& key, const glm::vec3& vec) const
{
	GLuint location = glGetUniformLocation(id, key.c_str());
	glUniform3fv(location, 1, glm::value_ptr(vec));
}

GLuint Shader::GetId() const
{
	return id;
}

Shader::~Shader()
{
	glDeleteProgram(id);
}

void Shader::move(Shader&& other)
{
	this->id = other.id; other.id = 0;
}
