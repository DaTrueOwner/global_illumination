#pragma once
#include <string>
#include "glad/glad.h"
#include "NonCopyable.h"
#include "Material.h"
#include <vector>
#include "glm/vec4.hpp"
#include <glm/mat4x4.hpp>

#define STR_(X) #X
#define STR(X) STR_(X)

#define RES(X) STR(RESOURCE_PATH) X 

struct ConstantReplaceMapping
{
	std::string Constant;
	std::string ReplaceWith;
};

enum class Std430
{
	Guint,
	Gint,
	Gfloat,
	Gvec4,
	Gvec3,
	Gvec2,
	Gmat3,
	Gmat4
};

template <typename TArray>
constexpr GLuint LayoutStd430Struct(const TArray& structLayoutArray)
{
	GLuint size = 0;
	GLuint baseAlignment = 0;
	// Rule 9
	for (int i = 0; i< sizeof(structLayoutArray) / sizeof(structLayoutArray[0]); i++)
	{
		Std430 member = structLayoutArray[i];
		switch (member)
		{
		case Std430::Gfloat: baseAlignment = std::max(4u, baseAlignment); size += 4u; break; // Rule 1
		case Std430::Gint:	 baseAlignment = std::max(4u, baseAlignment); size += 4u; break; // Rule 1
		case Std430::Guint:  baseAlignment = std::max(4u, baseAlignment); size += 4u; break; // Rule 1
		case Std430::Gvec2:  baseAlignment = std::max(8u, baseAlignment); size += 8u; break; // Rule 2
		case Std430::Gvec3:  baseAlignment = std::max(16u, baseAlignment); size += 12u; break; // Rule 3.
		case Std430::Gvec4:  baseAlignment = std::max(16u, baseAlignment); size += 16u; break; // Rule 2
		case Std430::Gmat3:  baseAlignment = std::max(3 * 16u, baseAlignment); size += 3 * 12u; break; // Rule 5
		case Std430::Gmat4:  baseAlignment = std::max(4 * 16u, baseAlignment); size += 4 * 16u; break; // Rule 5
		}
	}

	// size has to be a multiple of baseAlignment.
	// Ie vec3,vec3,float => baseAlignment == 16; size==28 => 28 / 16 == 1 => 1 + 1 = 2 => 2*16 == 32.
	return baseAlignment == size ? size : (GLuint(size / baseAlignment) + 1) * baseAlignment;

}

// TODO: Make sure that if shader is destroy, all corresponding instances are also destroyed / marked invalid.
class Shader
{
public:
	MAKE_NO_COPY(Shader)
	MAKE_SIMPLE_MOVE(Shader)
	Shader(const std::string& vertexPath, const std::string& fragmentPath,
	       const std::vector<ConstantReplaceMapping>& crMap = {});
	Shader(const std::string& vertexPath, const std::string& geomPath, const std::string& fragmentPath,
	       const std::vector<ConstantReplaceMapping>& crMap = {});
	Shader(const std::string& compPath, const std::vector<ConstantReplaceMapping>& crMap = {});
	void Use() const { glUseProgram(id); } 

	void Set(const std::string& key, float value) const;
	void Set(const std::string& key, GLuint value) const;

	void Set(const std::string& key, GLint value) const;
	void Set(const std::string& key, const glm::mat4& mat) const;
	void Set(const std::string& key, const glm::mat3& mat) const;
	void Set(const std::string& key, const glm::vec3& vec) const;
	GLuint GetId() const;
	

	~Shader();

private:
	GLuint id = 0;
	
	void move(Shader&&);

};
