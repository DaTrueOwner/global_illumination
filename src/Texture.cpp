#include "Texture.h"


Texture::Texture(GLuint width, GLuint height, GLenum internalFormat, GLenum format, GLenum type, const void* data,
	GLenum minFilter, GLenum magFilter, GLenum wrapS, GLenum wrapT) : Texture(width,height,internalFormat,format,type,data)
{
	SetTexParameter(minFilter, magFilter, wrapS, wrapT);
}

Texture::Texture(GLuint width, GLuint height, GLuint depth, GLenum internalFormat, GLenum format, GLenum type,
	const void* data, GLenum minFilter, GLenum magFilter, GLenum wrapS, GLenum wrapT) : Texture(width, height, width, internalFormat, format, type, data)
{
	SetTexParameter(minFilter, magFilter, wrapS, wrapT);
}

Texture::Texture(GLuint width, GLuint height, GLuint depth, GLenum internalFormat, GLenum format, GLenum type,
	const void* data)
{
	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_3D, id);
	glTexImage3D(GL_TEXTURE_3D, 0, internalFormat, width, height, depth, 0, format, type, data);
	textureType = GL_TEXTURE_3D;
}


void Texture::GenerateMipMaps() const
{
	glBindTexture(textureType, id);
	glGenerateMipmap(textureType);
}

Texture::Texture(GLuint width, GLuint height, GLenum internalFormat, GLenum format, GLenum type, const void* data)
{
	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_2D, id);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, type, data);
	textureType = GL_TEXTURE_2D;
}

void Texture::SetTexParameter(GLenum minFilter, GLenum magFilter, GLenum wrapS, GLenum wrapT) const
{
	glBindTexture(textureType, id);
	glTexParameteri(textureType, GL_TEXTURE_MAG_FILTER, magFilter);
	glTexParameteri(textureType, GL_TEXTURE_MIN_FILTER, minFilter);
	glTexParameteri(textureType, GL_TEXTURE_WRAP_S, wrapS);
	glTexParameteri(textureType, GL_TEXTURE_WRAP_T, wrapT);
}

Texture::~Texture()
{
	glDeleteTextures(1, &id);
}

void Texture::move(Texture&& other)
{
	this->id = other.id;
	this->textureType = other.textureType;
	other.id = 0;
}
