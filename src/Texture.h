#pragma once
#include "glad/glad.h"
#include "NonCopyable.h"
#include <utility>

class Texture 
{
public:
	MAKE_NO_COPY(Texture)
	MAKE_SIMPLE_MOVE(Texture)
	Texture() = default;
	Texture(GLuint width, GLuint height, GLenum internalFormat, GLenum format, GLenum type, const void* data);
	Texture(GLuint width, GLuint height, GLenum internalFormat, GLenum format, GLenum type, const void* data,
		GLenum minFilter, GLenum magFilter, GLenum wrapS, GLenum wrapT);
	Texture(GLuint width, GLuint height, GLuint depth, GLenum internalFormat, GLenum format, GLenum type, const void* data);
	Texture(GLuint width, GLuint height, GLuint depth, GLenum internalFormat, GLenum format, GLenum type, const void* data,
		GLenum minFilter, GLenum magFilter, GLenum wrapS, GLenum wrapT);
	void GenerateMipMaps() const;
	void SetTexParameter(GLenum minFilter, GLenum magFilter, GLenum wrapS, GLenum wrapT) const;
	/*Texture(GLuint width, GLuint height, GLuint depth, GLenum internalFormat, GLenum format, GLenum type, const void* data,
	        GLenum magFilter, GLenum minFilter, GLenum wrapS, GLenum wrapT);*/
	~Texture();
    void Bind() const
    {
        glBindTexture(textureType,id);
    }
private:
	GLuint id = 0;
	GLenum textureType;
	void move(Texture&& other);
};