#pragma once
#include "glad/glad.h"
#include "NonCopyable.h"
#include <utility>

class VertexArray
{
public:
	MAKE_NO_COPY(VertexArray)
	MAKE_SIMPLE_MOVE(VertexArray)
	VertexArray() {
		glGenVertexArrays(1, &id);
	}
	void Bind() const{
		glBindVertexArray(id);
	}
	~VertexArray() { glDeleteVertexArrays(1, &id); }
	
private:
	GLuint id;
	void move(VertexArray&& other);


};

