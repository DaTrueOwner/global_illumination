#include "glad/glad.h"
#include "GLFW/glfw3.h"
#include "App.h"
#include <memory>
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include "SceneChooser.h"
#include <iostream>
#include <chrono>
#include "Material.h" // To initialize the static variable.

std::unique_ptr<App> app = std::make_unique<App>();

// From https://learnopengl.com/In-Practice/Debugging; License: CC BY-NC 4.0
void APIENTRY glDebugOutput(GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar *message,
	const void *userParam)
{
	// ignore non-significant error/warning codes
	if (id == 131185) return;
	std::cout << "------ NOTE ---------" << std::endl;
	std::cout << "Debug message (" << id << "): " << message << std::endl;

	switch (source)
	{
	case GL_DEBUG_SOURCE_API:             std::cout << "Source: API"; break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   std::cout << "Source: Window System"; break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER: std::cout << "Source: Shader Compiler"; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:     std::cout << "Source: Third Party"; break;
	case GL_DEBUG_SOURCE_APPLICATION:     std::cout << "Source: Application"; break;
	case GL_DEBUG_SOURCE_OTHER:           std::cout << "Source: Other"; break;
	} std::cout << std::endl;

	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:               std::cout << "Type: Error"; break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cout << "Type: Deprecated Behaviour"; break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cout << "Type: Undefined Behaviour"; break;
	case GL_DEBUG_TYPE_PORTABILITY:         std::cout << "Type: Portability"; break;
	case GL_DEBUG_TYPE_PERFORMANCE:         std::cout << "Type: Performance"; break;
	case GL_DEBUG_TYPE_MARKER:              std::cout << "Type: Marker"; break;
	case GL_DEBUG_TYPE_PUSH_GROUP:          std::cout << "Type: Push Group"; break;
	case GL_DEBUG_TYPE_POP_GROUP:           std::cout << "Type: Pop Group"; break;
	case GL_DEBUG_TYPE_OTHER:               std::cout << "Type: Other"; break;
	} std::cout << std::endl;

	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:         
		std::cout << "Severity: high"; 
#ifdef _MSC_VER
		__debugbreak();
#endif
		break;
	case GL_DEBUG_SEVERITY_MEDIUM:       std::cout << "Severity: medium"; break;
	case GL_DEBUG_SEVERITY_LOW:          std::cout << "Severity: low"; break;
	case GL_DEBUG_SEVERITY_NOTIFICATION: std::cout << "Severity: notification"; break;
	} std::cout << std::endl;
	std::cout << "------ END ---------" << std::endl << std::endl;
}
int main()
{
	glfwInit();
    // create the window
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_FALSE);
#ifdef DEBUG
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
#endif
	GLFWwindow* window = glfwCreateWindow(600, 400, "Global Illumination Scenes", nullptr, nullptr);
	glfwMakeContextCurrent(window);
	if (!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress))) {
		throw std::runtime_error("Couldn't load OpenGL");
	}

	if (!window) throw std::runtime_error("Couldn't create window and context!");

	const unsigned char* openglVersion = glGetString(GL_VERSION);
	const unsigned char* glslVersion = glGetString(GL_SHADING_LANGUAGE_VERSION);
	printf("--- Information ---\n- OpenGL Version: %s\n- Shading Version: %s\n-------------------\n", openglVersion,glslVersion);

#ifdef DEBUG
	if (GL_CONTEXT_FLAG_DEBUG_BIT)
	{
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(&glDebugOutput, nullptr);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
	}
#endif
    
	Material::Init();

	ImGui::CreateContext();
	ImGui_ImplGlfw_InitForOpenGL(window, false);
	ImGui_ImplOpenGL3_Init("#version 430 core");
	SceneChooser chooser;
	glfwSetWindowSizeCallback(window,[](GLFWwindow* window, int width, int height)
	{
		glViewport(0, 0, width, height);
		app->HandleResize(width, height);
	});

	// run the main loop
    bool running = true;
	glClearColor(0, 0, 0, 1);
	glEnable(GL_DEPTH_TEST);
	ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0);
	double lastTime = 0;
    while (!glfwWindowShouldClose(window))
    {
		
		auto now = std::chrono::high_resolution_clock::now();
		glfwPollEvents();
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		ImGui::ShowMetricsWindow();
		try {
			chooser.Choose(app);
			app->Update(); }
		// TODO: Implement better exception support
		catch(const std::runtime_error& error)
		{
			printf("ERROR OCCURED: %s\n", error.what());
		}
		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
		glfwSwapBuffers(window);
		auto then = std::chrono::high_resolution_clock::now();
		app->SetTimeForThisFrame(std::chrono::duration_cast<std::chrono::milliseconds>(then - now).count());
    }
	ImGui::PopStyleVar();
	ImGui_ImplOpenGL3_Shutdown();
	ImGui::DestroyContext();
	ImGui_ImplGlfw_Shutdown();
	glfwTerminate();
    return 0;
}
